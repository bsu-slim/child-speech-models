# README #

This repository contains trained adult and child speech models for DeepSpeech2.  The models are compatible with version 1.0 of https://github.com/SeanNaren/deepspeech.pytorch.  The speech corpora used are librispeech_train (http://www.openslr.org/12) and CMU_Kids (https://catalog.ldc.upenn.edu/LDC97S63)

## Models ##

### librispeech.pth: trained on librispeech training set (adult speech) ###
```
Model name:          librispeech.pth
DeepSpeech version:  0.0.1

Recurrent Neural Network Properties
  RNN Type:          gru
  RNN Layers:        5
  RNN Size:          800
  Classes:           29

Model Features
  Labels:            _'ABCDEFGHIJKLMNOPQRSTUVWXYZ 
  Sample Rate:       16000
  Window Type:       hamming
  Window Size:       0.02
  Window Stride:     0.01

Training Information
  Epochs:            24
  Current Loss:      0.228
  Current CER:       7.306
  Current WER:       22.147
```

### cmu-kids.pth: trained on CMU_kids training set (child speech) ###
```
Model name:          cmu-kids.pth
DeepSpeech version:  0.0.1

Recurrent Neural Network Properties
  RNN Type:          gru
  RNN Layers:        5
  RNN Size:          800
  Classes:           29

Model Features
  Labels:            _'ABCDEFGHIJKLMNOPQRSTUVWXYZ 
  Sample Rate:       16000
  Window Type:       hamming
  Window Size:       0.02
  Window Stride:     0.01

Training Information
  Epochs:            25
  Current Loss:      3.765
  Current CER:       19.198
  Current WER:       54.409
```

### libri_transfer_cmu.pth: trained on librispeech training set, fine-tuned using CMU_kids training set ###
```
Model name:          libri_transfer_cmu.pth
DeepSpeech version:  0.0.1

Recurrent Neural Network Properties
  RNN Type:          gru
  RNN Layers:        5
  RNN Size:          800
  Classes:           29

Model Features
  Labels:            _'ABCDEFGHIJKLMNOPQRSTUVWXYZ 
  Sample Rate:       16000
  Window Type:       hamming
  Window Size:       0.02
  Window Stride:     0.01

Training Information
  Epochs:            21
  Current Loss:      0.260
  Current CER:       9.967
  Current WER:       25.213
```

## Who do I talk to? ##

* Eric Booth - ericbooth76@gmail.com